// Created by Dmitriy Chepurkin, All Rights Reserved

#include "Game/SnakeGameModeBase.h"
#include "Player/SnakePlayerPawn.h"

DEFINE_LOG_CATEGORY_STATIC(LogSnakeGameMode, All, All);

ASnakeGameModeBase::ASnakeGameModeBase()
{
	DefaultPawnClass = ASnakePlayerPawn::StaticClass();
}

void ASnakeGameModeBase::StartGame() const
{
	OnGameStart.Broadcast();
}

void ASnakeGameModeBase::OnAddElement(const int32 ElementsCount) const
{
	if(ElementsCount >= SnakeElementsToWin)
	{
		OnGameOver.Broadcast();
		UE_LOG(LogSnakeGameMode, Display, TEXT("YOU WIN"));
	}
}

void ASnakeGameModeBase::GameOver()
{	
	OnGameOver.Broadcast();
	SaveBestScore();
	UE_LOG(LogSnakeGameMode, Display, TEXT("YOU LOSE"));
}
