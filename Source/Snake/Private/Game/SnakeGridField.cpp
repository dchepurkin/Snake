// Created by Dmitriy Chepurkin, All Rights Reserved

#include "Game/SnakeGridField.h"

#include "DrawDebugHelpers.h"
#include "SnakeGameModeBase.h"
#include "Interactable/SnakeFood.h"
#include "Interactable/SnakeWall.h"

DEFINE_LOG_CATEGORY_STATIC(LogGridField, All, All);

ASnakeGridField::ASnakeGridField()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
}

void ASnakeGridField::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	for(int8 i = 0; i < LevelSize.XSize; ++i)
	{
		if(i == 0 || i == LevelSize.XSize - 1)
		{
			for(int8 j = 0; j < LevelSize.YSize; ++j)
			{
				SpawnWall(i, j);
			}
		}
		else
		{
			SpawnWall(i, 0);
			SpawnWall(i, LevelSize.YSize - 1);
		}
	}
}

void ASnakeGridField::BeginPlay()
{
	Super::BeginPlay();

	if(GetWorld())
	{
		if(const auto GameMode = GetWorld()->GetAuthGameMode<ASnakeGameModeBase>())
		{
			GameMode->OnGameStart.AddDynamic(this, &ThisClass::StartGame);
			GameMode->OnGameOver.AddDynamic(this, &ThisClass::OnGameEnd);
			GameMode->SetElementsCountToWin((LevelSize.XSize - 2) * (LevelSize.YSize - 2) - 2);
		}
	}
}

void ASnakeGridField::SpawnWall(const int32 InX, const int32 InY)
{
	const FVector SpawnLocation{WallSize * InX, WallSize * InY, 0.f};

	const auto ActorComponent = AddComponentByClass(UChildActorComponent::StaticClass(), false, FTransform(SpawnLocation), false);
	const auto ChildActorComponent = Cast<UChildActorComponent>(ActorComponent);
	if(!ChildActorComponent) return;

	ChildActorComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	ChildActorComponent->SetChildActorClass(WallClass);
	ChildActorComponent->RegisterComponent();
}

void ASnakeGridField::StartGame()
{
	SpawnFood();
	InitDynamicWallSpawner();
}

void ASnakeGridField::InitDynamicWallSpawner()
{
	GetWorldTimerManager().SetTimer(SpawnWallTimerHandle, this, &ThisClass::SpawnDynamicWall, GetSpawnWallDelay());
}

void ASnakeGridField::OnGameEnd()
{
	if(Food)
	{
		Food->Destroy();
	}

	RemoveWalls();
}

void ASnakeGridField::SpawnFood()
{
	if(!GetWorld()) return;
	if(!FoodClass)
	{
		UE_LOG(LogGridField, Warning, TEXT("Food Class is Empty"))
	}

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	Food = GetWorld()->SpawnActor<ASnakeFood>(FoodClass, GetBonusSpawnLocation(), FRotator::ZeroRotator, SpawnParameters);
	if(!Food) return;

	Food->OnMove.AddUObject(this, &ThisClass::MoveFood);
}

void ASnakeGridField::MoveFood(AActor* Bonus) const
{
	if(!Bonus) return;
	Bonus->SetActorLocation(GetBonusSpawnLocation());
}

FVector ASnakeGridField::GetBonusSpawnLocation() const
{
	FHitResult HitResult;
	FVector TracePoint;

	do
	{
		const auto SpawnX = FMath::RandRange(1, LevelSize.XSize - 2) * WallSize;
		const auto SpawnY = FMath::RandRange(1, LevelSize.YSize - 2) * WallSize;
		TracePoint = GetActorTransform().TransformPosition(FVector(SpawnX, SpawnY, 0.f));

		GetWorld()->LineTraceSingleByChannel(HitResult, TracePoint + FVector(0.f, 0.f, WallSize), TracePoint, ECC_Visibility);
	}
	while(HitResult.bBlockingHit);

	return TracePoint;
}

float ASnakeGridField::GetSpawnWallDelay() const
{
	return FMath::RandRange(MinDelayForSpawnNewWall, MaxDelayForSpawnNewWall);
}

void ASnakeGridField::SpawnDynamicWall()
{
	InitDynamicWallSpawner();

	constexpr int8 MaxSpawnTry = 20;
	bool CanSpawn = true;
	FVector SpawnPoint;
	FHitResult HitResult;

	for(int8 SpawnTry = 0; SpawnTry < MaxSpawnTry; ++SpawnTry)
	{
		CanSpawn = true;

		const auto SpawnX = FMath::RandRange(2, LevelSize.XSize - 3);
		const auto SpawnY = FMath::RandRange(2, LevelSize.YSize - 3);
		SpawnPoint = GetActorTransform().TransformPosition(FVector(SpawnX * WallSize, SpawnY * WallSize, 0.f));

		for(int32 i = SpawnX - 1; i <= SpawnX + 1; ++i)
		{
			for(int32 j = SpawnY - 1; j <= SpawnY + 1; ++j)
			{
				const auto TracePoint = GetActorTransform().TransformPosition(FVector(i * WallSize, j * WallSize, 0.f));
				GetWorld()->LineTraceSingleByChannel(HitResult, TracePoint + FVector(0.f, 0.f, WallSize), TracePoint, ECC_Visibility);

				if(HitResult.bBlockingHit)
				{
					CanSpawn = false;
					break;
				}
			}
			if(!CanSpawn) break;
		}
		if(CanSpawn) break;
	}

	if(!CanSpawn) return;

	const auto NewWall = GetWorld()->SpawnActorDeferred<ASnakeWall>(WallClass, FTransform(SpawnPoint), nullptr, nullptr,
	                                                                ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	if(NewWall)
	{
		DynamicWalls.Add(NewWall);
		NewWall->SetDestructable(true);
		NewWall->OnMove.AddUObject(this, &ThisClass::DestroyWall);
		NewWall->FinishSpawning(FTransform(SpawnPoint));
	}
}

void ASnakeGridField::DestroyWall(AActor* Wall)
{
	if(!Wall) return;

	if(DynamicWalls.Contains(Wall)) DynamicWalls.Remove(Wall);
	Wall->Destroy();
}

void ASnakeGridField::RemoveWalls()
{
	GetWorldTimerManager().ClearTimer(SpawnWallTimerHandle);

	for(const auto Wall : DynamicWalls)
	{
		if(IsValid(Wall)) Wall->Destroy();
	}

	DynamicWalls.Empty();
}
