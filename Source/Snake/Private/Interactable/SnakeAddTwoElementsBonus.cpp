// Created by Dmitriy Chepurkin, All Rights Reserved

#include "Interactable/SnakeAddTwoElementsBonus.h"

#include "SnakePlayerPawn.h"

void USnakeAddTwoElementsBonus::UseBonus(ASnakePlayerPawn* Player)
{
	if(!Player) return;

	Player->AddSnakeElement(ElementsCountToAdd);
}
