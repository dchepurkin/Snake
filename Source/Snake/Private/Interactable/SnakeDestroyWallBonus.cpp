// Created by Dmitriy Chepurkin, All Rights Reserved


#include "Interactable/SnakeDestroyWallBonus.h"

#include "SnakePlayerPawn.h"

void USnakeDestroyWallBonus::UseBonus(ASnakePlayerPawn* Player)
{
	if(!Player) return;

	Player->ApplyDestroyWallBonus(BonusEffectTime);
}
