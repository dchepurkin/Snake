// Created by Dmitriy Chepurkin, All Rights Reserved

#include "Interactable/SnakeFood.h"

#include "SnakeBodyElement.h"
#include "SnakeCircleProgressBar.h"
#include "SnakePlayerPawn.h"
#include "Interactable/SnakeFoodBonus.h"
#include "Kismet/GameplayStatics.h"

void ASnakeFood::BeginPlay()
{
	Super::BeginPlay();

	SetVisual(FoodData);
}

void ASnakeFood::Interact(ASnakeBodyElement* SnakeElement)
{
	if(!SnakeElement) return;

	UGameplayStatics::PlaySound2D(GetWorld(), Bonus ? Bonus->GetBonusData().EatSound : FoodData.EatSound);
	SnakeElement->Eat(GetScore(), true);
	UseBonus(SnakeElement);
	Move();	
}

void ASnakeFood::Move()
{
	RemoveBonus();
	TryToAddNewBonus();
	UpdateLifeTimeWidget();

	Super::Move();
}

void ASnakeFood::UseBonus(ASnakeBodyElement* SnakeElement) const
{
	if(!Bonus) return;

	const auto Player = SnakeElement->GetOwner<ASnakePlayerPawn>();
	if(!Player) return;

	Bonus->UseBonus(Player);
}

void ASnakeFood::TryToAddNewBonus()

{
	if(BonusesClasses.Num() == 0) return;

	const auto BonusClass = BonusesClasses.FindByPredicate([this](const FBonusInfo& BonusInfo)
	{
		return BonusInfo.BonusChance >= FMath::RandRange(0.f, 1.f);
	});

	if(!BonusClass) return;

	Bonus = NewObject<USnakeFoodBonus>(this, BonusClass->BonusClass);

	if(Bonus)
	{
		SetVisual(Bonus->GetBonusData());
	}
}

void ASnakeFood::RemoveBonus()
{
	if(!Bonus) return;

	Bonus->ConditionalBeginDestroy();
	Bonus = nullptr;
	SetVisual(FoodData);
}

void ASnakeFood::SetVisual(const FFoodData& Data) const
{
	if(!StaticMesh) return;

	StaticMesh->SetStaticMesh(Data.Mesh);
	StaticMesh->SetVectorParameterValueOnMaterials(Data.ColorParameterName, FVector(Data.Color));
}

int32 ASnakeFood::GetScore() const
{
	int32 BonusScore = 0;
	if(Bonus) BonusScore = Bonus->GetBonusData().Score;

	return (FoodData.Score + BonusScore) * GetLifeTimePercent();
}

void ASnakeFood::UpdateLifeTimeWidget() const
{
	LifeTimeWidget->SetMaterial(Bonus ? Bonus->GetBonusData().LifeTimeProgressBarMaterial : FoodData.LifeTimeProgressBarMaterial);
}
