// Created by Dmitriy Chepurkin, All Rights Reserved

#include "Interactable/SnakeInteractable.h"

#include "SnakeBodyElement.h"
#include "SnakeCircleProgressBar.h"
#include "Components/WidgetComponent.h"

ASnakeInteractable::ASnakeInteractable()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetCollisionResponseToAllChannels(ECR_Block);
	StaticMesh->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Overlap);
	SetRootComponent(StaticMesh);

	LifeTimeWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("LifeTimeWidget");
	LifeTimeWidgetComponent->SetupAttachment(StaticMesh);
	LifeTimeWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
	LifeTimeWidgetComponent->SetDrawAtDesiredSize(true);
}

void ASnakeInteractable::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	LifeTick(DeltaSeconds);
}

void ASnakeInteractable::BeginPlay()
{
	Super::BeginPlay();

	CurrentLifeTime = LifeTime;

	if(StaticMesh)
	{
		StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &ThisClass::OnBeginOverlap);
	}

	if(LifeTimeWidgetComponent)
	{
		LifeTimeWidget = Cast<USnakeCircleProgressBar>(LifeTimeWidgetComponent->GetWidget());
		if(LifeTimeWidget)
		{
			OnLifeTimeChanged.AddUObject(LifeTimeWidget, &USnakeCircleProgressBar::SetPercent);
			LifeTimeWidget->SetMaterial(FoodData.LifeTimeProgressBarMaterial);
		}
	}
}

void ASnakeInteractable::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                                        AActor* OtherActor,
                                        UPrimitiveComponent* OtherComp,
                                        int32 OtherBodyIndex,
                                        bool bFromSweep,
                                        const FHitResult& SweepResult)
{
	if(!OtherActor) return;

	if(const auto SnakeElement = Cast<ASnakeBodyElement>(OtherActor))
	{
		Interact(SnakeElement);
	}
}

void ASnakeInteractable::LifeTick(const float DeltaSeconds)
{
	CurrentLifeTime = FMath::Clamp(CurrentLifeTime - DeltaSeconds, 0.f, LifeTime);
	if(FMath::IsNearlyZero(CurrentLifeTime))
	{
		Move();
	}

	OnLifeTimeChanged.Broadcast(CurrentLifeTime / LifeTime);
}

void ASnakeInteractable::Move()
{
	OnMove.Broadcast(this);
	CurrentLifeTime = LifeTime;
}
