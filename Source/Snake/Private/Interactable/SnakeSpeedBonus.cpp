// Created by Dmitriy Chepurkin, All Rights Reserved


#include "Interactable/SnakeSpeedBonus.h"

#include "SnakePlayerPawn.h"

void USnakeSpeedBonus::UseBonus(ASnakePlayerPawn* Player)
{
	if(!Player) return;

	Player->ApplySpeedBonus(SpeedFactor, BonusEffectTime);
}
