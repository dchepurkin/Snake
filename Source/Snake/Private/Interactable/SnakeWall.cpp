// Created by Dmitriy Chepurkin, All Rights Reserved

#include "Interactable/SnakeWall.h"

#include "SnakeBodyElement.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"

void ASnakeWall::SetDestructable(const bool Destructable)
{
	bDestructable = Destructable;
}

void ASnakeWall::BeginPlay()
{
	if(!bDestructable && LifeTimeWidgetComponent)
	{
		LifeTimeWidgetComponent->DestroyComponent();
	}

	Super::BeginPlay();
}

void ASnakeWall::Interact(ASnakeBodyElement* SnakeElement)
{
	if(!SnakeElement) return;

	if(bDestructable && SnakeElement->CanDestroyWall())
	{
		UGameplayStatics::PlaySound2D(GetWorld(), FoodData.EatSound);
		SnakeElement->Eat(FoodData.Score, false);
		Destroy();
	}
	else
	{
		SnakeElement->GameOver();
	}
}
