// Created by Dmitriy Chepurkin, All Rights Reserved

#include "Player/SnakeBodyElement.h"

#include "SnakePlayerPawn.h"

ASnakeBodyElement::ASnakeBodyElement()
{
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetCollisionResponseToAllChannels(ECR_Block);
	StaticMesh->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Overlap);
	SetRootComponent(StaticMesh);
}

void ASnakeBodyElement::BeginPlay()
{
	Super::BeginPlay();
	PreviewLocation = GetActorLocation();

	if(IsHead() && StaticMesh)
	{
		StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &ThisClass::OnBeginOverlap);
	}
}

void ASnakeBodyElement::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent,
                                       AActor* OtherActor,
                                       UPrimitiveComponent* OtherComp,
                                       int32 OtherBodyIndex,
                                       bool bFromSweep,
                                       const FHitResult& SweepResult)
{
	if(OtherActor && OtherActor->IsA(StaticClass()))
	{
		GameOver();
	}
}

void ASnakeBodyElement::SetHead(const bool IsHead)
{
	bHead = IsHead;
}

void ASnakeBodyElement::Move(const FVector& NewLocation)
{
	PreviewLocation = GetActorLocation();
	SetActorLocation(NewLocation);
}

void ASnakeBodyElement::Eat(const int32 ScoreCount, const bool AddElement) const
{
	OnEat.Broadcast(ScoreCount, AddElement);
}

void ASnakeBodyElement::GameOver()
{
	Destroy();
	OnCollision.Broadcast();
}

void ASnakeBodyElement::SetColor(const FSnakeColorData& ColorData) const
{
	if(!StaticMesh) return;

	StaticMesh->SetVectorParameterValueOnMaterials(ColorParameterName, FVector(IsHead() ? ColorData.HeadColor : ColorData.BodyColor));
}

bool ASnakeBodyElement::CanDestroyWall() const
{
	const auto PlayerPawn = GetOwner<ASnakePlayerPawn>();
	return PlayerPawn && PlayerPawn->HaveDestroyWallBonus();
}
