// Created by Dmitriy Chepurkin, All Rights Reserved

#include "Player/SnakePlayerPawn.h"

#include "SnakeBodyElement.h"
#include "SnakeGameModeBase.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogPlayerPawn, All, All);

ASnakePlayerPawn::ASnakePlayerPawn()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;
}

void ASnakePlayerPawn::BeginPlay()
{
	Super::BeginPlay();

	CurrentLifeTime = SnakeLifeTime;
	CurrentMovementDirection = StartMovementDirection;
	SetActorTickInterval(StartMoveTickInterval);
	AddSnakeElement(StartElementsCount);
}

void ASnakePlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if(!PlayerInputComponent) return;

	PlayerInputComponent->BindAction("StartGame", IE_Pressed, this, &ThisClass::StartGame);

	PlayerInputComponent->BindAxis("MoveForward", this, &ThisClass::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ThisClass::MoveRight);
}

void ASnakePlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	bIsGameOver
		? DestroySnake()
		: MoveTick();

	if(bIsHaveSpeedBonus) SpeedBonusTick(DeltaTime);
	if(bIsHaveDestroyWallBonus) DestroyWallBonusTick(DeltaTime);
}

void ASnakePlayerPawn::SetMovementDirection(const EMovementDirection NewDirection)
{
	if(!bIsGameStarted) return;

	TMap<EMovementDirection, EMovementDirection> CanceledDirections{
		{EMovementDirection::MD_Up, EMovementDirection::MD_Down},
		{EMovementDirection::MD_Down, EMovementDirection::MD_Up},
		{EMovementDirection::MD_Left, EMovementDirection::MD_Right},
		{EMovementDirection::MD_Right, EMovementDirection::MD_Left},
	};

	if(CurrentMovementDirection != CanceledDirections[NewDirection] && PreviewMovementDirection != CanceledDirections[NewDirection])
	{
		CurrentMovementDirection = NewDirection;
	}
}

void ASnakePlayerPawn::MoveForward(const float AxisValue)
{
	if(FMath::IsNearlyZero(AxisValue)) return;

	SetMovementDirection(AxisValue > 0 ? EMovementDirection::MD_Up : EMovementDirection::MD_Down);
}

void ASnakePlayerPawn::MoveRight(const float AxisValue)
{
	if(FMath::IsNearlyZero(AxisValue)) return;

	SetMovementDirection(AxisValue > 0 ? EMovementDirection::MD_Right : EMovementDirection::MD_Left);
}

void ASnakePlayerPawn::MoveTick()
{
	if(!BodyElements[0]) return;

	const auto NewHeadLocation = BodyElements[0]->GetActorLocation() + DirectionVectors[CurrentMovementDirection] * BodyElementSize;
	BodyElements[0]->Move(NewHeadLocation);

	for(int32 i = 1; i < BodyElements.Num(); ++i)
	{
		if(!BodyElements[i] || !BodyElements[i - 1]) continue;
		BodyElements[i]->Move(BodyElements[i - 1]->GetPreviewLocation());
	}

	PreviewMovementDirection = CurrentMovementDirection;
}

void ASnakePlayerPawn::DestroySnake()
{
	if(BodyElements.Num() == 0)
	{
		SetActorTickEnabled(false);
	}
	else
	{
		const auto LastIndex = BodyElements.Num() - 1;
		if(BodyElements[LastIndex])
		{
			BodyElements[LastIndex]->Destroy();
		}
		BodyElements.RemoveAt(LastIndex);
	}
}

void ASnakePlayerPawn::AddSnakeElement(const int32 ElementsCount)
{
	if(!GetWorld()) return;

	for(int32 i = 0; i < ElementsCount; ++i)
	{
		const auto NewElement = GetWorld()->SpawnActorDeferred<ASnakeBodyElement>(BodyElementClass,
		                                                                          FTransform::Identity,
		                                                                          this,
		                                                                          this,
		                                                                          ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

		if(!NewElement) return;

		NewElement->SetHead(BodyElements.Add(NewElement) == 0);
		NewElement->SetColor(bIsHaveDestroyWallBonus ? DestroyWallSnakeColor : DefaultSnakeColor);

		if(NewElement->IsHead())
		{
			NewElement->OnEat.AddUObject(this, &ThisClass::OnEat);
			NewElement->OnCollision.AddUObject(this, &ThisClass::OnGameOver);
		}
		NewElement->FinishSpawning(FTransform::Identity);

		OnAddElement.Broadcast(BodyElements.Num());
	}
}

void ASnakePlayerPawn::OnEat(const int32 ScoreCount, const bool AddElement)
{
	if(BodyElements.Num() == 0 || !BodyElements[BodyElements.Num() - 1]) return;

	if(AddElement)
	{
		AddSnakeElement();
		CurrentLifeTime = SnakeLifeTime;
	}

	CurrentScore += ScoreCount;
}

void ASnakePlayerPawn::StartGame()
{
	if(bIsGameStarted) return;

	bIsGameStarted = true;
	SetActorTickEnabled(true);
	GetWorldTimerManager().SetTimer(LifeTimeTimerHandle, this, &ThisClass::SnakeLifeTimeTick, 0.01f, true);

	if(!GetWorld()) return;

	if(const auto GameMode = GetWorld()->GetAuthGameMode<ASnakeGameModeBase>())
	{
		GameMode->StartGame();
		GameMode->OnGameOver.AddDynamic(this, &ThisClass::OnGameOver);
		OnAddElement.AddUObject(GameMode, &ASnakeGameModeBase::OnAddElement);
		BodyElements[0]->OnCollision.AddUObject(GameMode, &ASnakeGameModeBase::GameOver);
	}	
}

void ASnakePlayerPawn::UpdateSnakeColor()
{
	for(const auto SnakeElement : BodyElements)
	{
		if(!SnakeElement) continue;
		SnakeElement->SetColor(bIsHaveDestroyWallBonus ? DestroyWallSnakeColor : DefaultSnakeColor);
	}
}

void ASnakePlayerPawn::OnGameOver()
{
	GetWorldTimerManager().ClearTimer(LifeTimeTimerHandle);

	UGameplayStatics::PlaySound2D(GetWorld(), DeathSound);
	bIsGameOver = true;
}

void ASnakePlayerPawn::ApplySpeedBonus(const float SpeedFactor, const float BonusTime)
{
	OnTakeSpeedBonus.Broadcast();

	bIsHaveSpeedBonus = true;
	SpeedBonusTime = BonusTime;
	SetActorTickInterval(FMath::Clamp(GetActorTickInterval() - SpeedFactor, MinMoveTickInterval, MaxMoveTickInterval));
}

void ASnakePlayerPawn::ApplyDestroyWallBonus(const float BonusTime)
{
	OnTakeDestroyWallBonus.Broadcast();

	bIsHaveDestroyWallBonus = true;
	DestroyWallBonusTime = BonusTime;
	UpdateSnakeColor();
}

void ASnakePlayerPawn::SpeedBonusTick(const float DeltaSeconds)
{
	SpeedBonusTime -= DeltaSeconds;

	if(SpeedBonusTime <= 0.f)
	{
		bIsHaveSpeedBonus = false;
		SpeedBonusTime = 0.f;
		SetActorTickInterval(StartMoveTickInterval);
	}
}

void ASnakePlayerPawn::DestroyWallBonusTick(const float DeltaSeconds)
{
	DestroyWallBonusTime -= DeltaSeconds;

	if(DestroyWallBonusTime <= 0.f)
	{
		bIsHaveDestroyWallBonus = false;
		DestroyWallBonusTime = 0.f;
		UpdateSnakeColor();
	}
}

void ASnakePlayerPawn::SnakeLifeTimeTick()
{
	CurrentLifeTime -= 0.01f;
	if(CurrentLifeTime <= 0.f)
	{
		if(const auto GameMode = GetWorld()->GetAuthGameMode<ASnakeGameModeBase>())
		{
			GameMode->GameOver();
		}
	}
}
