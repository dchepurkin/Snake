// Created by Dmitriy Chepurkin, All Rights Reserved

#include "UI/SnakeCircleProgressBar.h"

#include "Components/Image.h"

void USnakeCircleProgressBar::SetMaterial(UMaterialInstance* NewMaterial)
{
	if(!LifeTimeProgressBar) return;

	LifeTimeProgressBar->SetBrushFromMaterial(NewMaterial);
	CurrentMaterial = LifeTimeProgressBar->GetDynamicMaterial();
}

void USnakeCircleProgressBar::SetPercent(const float NewPercent) const
{
	if(!CurrentMaterial) return;
	
	CurrentMaterial->SetScalarParameterValue(PercentParameterName, NewPercent);
}
