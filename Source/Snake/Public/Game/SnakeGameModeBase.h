// Created by Dmitriy Chepurkin, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameModeBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameStartSignature);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameOverSignature);

UCLASS()
class SNAKE_API ASnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FOnGameStartSignature OnGameStart;

	UPROPERTY(BlueprintAssignable)
	FOnGameOverSignature OnGameOver;

	ASnakeGameModeBase();
	void StartGame() const;
	void SetElementsCountToWin(const int32 Count) { SnakeElementsToWin = Count; }
	void OnAddElement(const int32 ElementsCount) const;
	void GameOver();

	UFUNCTION(BlueprintImplementableEvent)
	void SaveBestScore();

private:
	int32 SnakeElementsToWin = 0;
};
