// Created by Dmitriy Chepurkin, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeGridField.generated.h"

class ASnakeFood;
class ASnakeWall;

USTRUCT(BlueprintType)
struct FGridSize
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, DisplayName="X", meta=(ClampMin=3))
	int32 XSize = 3;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, DisplayName="Y", meta=(ClampMin=3))
	int32 YSize = 3;
};

UCLASS()
class SNAKE_API ASnakeGridField : public AActor
{
	GENERATED_BODY()

public:
	ASnakeGridField();
	virtual void OnConstruction(const FTransform& Transform) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="GridField|Walls")
	TSubclassOf<ASnakeWall> WallClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="GridField|Walls")
	FGridSize LevelSize;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="GridField|Walls")
	float MinDelayForSpawnNewWall = 3.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="GridField|Walls")
	float MaxDelayForSpawnNewWall = 5.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="GridField|Walls", meta=(ClampMin=1.f))
	float WallSize = 50.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="GridField")
	TSubclassOf<ASnakeFood> FoodClass;

	virtual void BeginPlay() override;

private:
	FTimerHandle SpawnWallTimerHandle;

	UPROPERTY()
	ASnakeFood* Food = nullptr;

	UPROPERTY()
	TArray<AActor*> DynamicWalls;

	UFUNCTION()
	void StartGame();

	UFUNCTION()
	void OnGameEnd();

	void SpawnWall(const int32 InX, const int32 InY);
	void SpawnFood();	
	void MoveFood(AActor* Food) const;
	void SpawnDynamicWall();
	void InitDynamicWallSpawner();
	void DestroyWall(AActor* Wall);
	void RemoveWalls();

	float GetSpawnWallDelay() const;
	FVector GetBonusSpawnLocation() const;
};
