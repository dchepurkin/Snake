// Created by Dmitriy Chepurkin, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Interactable/SnakeFoodBonus.h"
#include "SnakeAddTwoElementsBonus.generated.h"

UCLASS(Blueprintable)
class SNAKE_API USnakeAddTwoElementsBonus : public USnakeFoodBonus
{
	GENERATED_BODY()

public:
	virtual void UseBonus(ASnakePlayerPawn* Player) override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 ElementsCountToAdd = 2;
};
