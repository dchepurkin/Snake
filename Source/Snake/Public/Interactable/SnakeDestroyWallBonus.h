// Created by Dmitriy Chepurkin, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Interactable/SnakeFoodBonus.h"
#include "SnakeDestroyWallBonus.generated.h"

UCLASS(Blueprintable)
class SNAKE_API USnakeDestroyWallBonus : public USnakeFoodBonus
{
	GENERATED_BODY()

public:
	virtual void UseBonus(ASnakePlayerPawn* Player) override;
	
};
