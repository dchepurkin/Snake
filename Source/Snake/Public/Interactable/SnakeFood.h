// Created by Dmitriy Chepurkin, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Interactable/SnakeInteractable.h"
#include "SnakeFood.generated.h"

class USnakeFoodBonus;

USTRUCT(BlueprintType)
struct FBonusInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<USnakeFoodBonus> BonusClass = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(ClampMin=0.f, ClampMax=1.f))
	float BonusChance = 0.f;
};

UCLASS(Blueprintable)
class SNAKE_API ASnakeFood : public ASnakeInteractable
{
	GENERATED_BODY()	

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FBonusInfo> BonusesClasses;

	virtual void BeginPlay() override;
	virtual void Interact(ASnakeBodyElement* SnakeElement) override;
	virtual void Move() override;

private:
	UPROPERTY(BlueprintReadOnly, meta=(AllowPrivateAccess = true))
	USnakeFoodBonus* Bonus;
	
	void UseBonus(ASnakeBodyElement* SnakeElement) const;
	void TryToAddNewBonus();
	void RemoveBonus();
	void SetVisual(const FFoodData& Data) const;
	int32 GetScore() const;
	void UpdateLifeTimeWidget() const;
};
