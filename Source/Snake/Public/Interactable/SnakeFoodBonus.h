// Created by Dmitriy Chepurkin, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "SnakeCoreTypes.h"
#include "SnakeFoodBonus.generated.h"

class ASnakePlayerPawn;

UCLASS(Abstract)
class SNAKE_API USnakeFoodBonus : public UObject
{
	GENERATED_BODY()

public:
	virtual void UseBonus(ASnakePlayerPawn* Player) {}
	const FFoodData& GetBonusData() const { return BonusData; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(ClampMin=1.f))
	float BonusEffectTime = 5.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FFoodData BonusData;
};
