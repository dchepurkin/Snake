// Created by Dmitriy Chepurkin, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "SnakeCoreTypes.h"
#include "GameFramework/Actor.h"
#include "SnakeInteractable.generated.h"

class ASnakeBodyElement;
class UWidgetComponent;
class USnakeCircleProgressBar;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnMoveSignature, AActor*)
DECLARE_MULTICAST_DELEGATE_OneParam(FOnLifeTimeChangedSignature, float)

UCLASS(Abstract, NotBlueprintable)
class SNAKE_API ASnakeInteractable : public AActor
{
	GENERATED_BODY()

public:
	FOnMoveSignature OnMove;
	FOnLifeTimeChangedSignature OnLifeTimeChanged;

	ASnakeInteractable();
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable)
	float GetLifeTimePercent() const { return CurrentLifeTime / LifeTime; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
	UWidgetComponent* LifeTimeWidgetComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FFoodData FoodData;

	UPROPERTY()
	USnakeCircleProgressBar* LifeTimeWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(ClampMin = 1.f))
	float LifeTime = 10.f;

	virtual void BeginPlay() override;
	virtual void Interact(ASnakeBodyElement* SnakeElement) {}
	virtual void Move();

private:
	float CurrentLifeTime = 0;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	                    AActor* OtherActor,
	                    UPrimitiveComponent* OtherComp,
	                    int32 OtherBodyIndex,
	                    bool bFromSweep,
	                    const FHitResult& SweepResult);

	void LifeTick(const float DeltaSeconds);
};
