// Created by Dmitriy Chepurkin, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Interactable/SnakeFoodBonus.h"
#include "SnakeSpeedBonus.generated.h"

UCLASS(Blueprintable)
class SNAKE_API USnakeSpeedBonus : public USnakeFoodBonus
{
	GENERATED_BODY()

public:
	virtual void UseBonus(ASnakePlayerPawn* Player) override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float SpeedFactor = 0.2f;
};
