// Created by Dmitriy Chepurkin, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Interactable/SnakeInteractable.h"
#include "SnakeWall.generated.h"

UCLASS(Blueprintable)
class SNAKE_API ASnakeWall : public ASnakeInteractable
{
	GENERATED_BODY()

public:	
	bool CanDestroy() const { return bDestructable; }
	void SetDestructable(const bool Destructable);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, DisplayName="Can Be Destroyed")
	bool bDestructable = false;

	virtual void BeginPlay() override;
	virtual void Interact(ASnakeBodyElement* SnakeElement) override;
};
