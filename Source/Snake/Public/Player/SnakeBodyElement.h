// Created by Dmitriy Chepurkin, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "SnakeCoreTypes.h"
#include "GameFramework/Actor.h"
#include "SnakeBodyElement.generated.h"

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnEatSignature, const int32, const bool);
DECLARE_MULTICAST_DELEGATE(FOnCollisionSignature);

UCLASS()
class SNAKE_API ASnakeBodyElement : public AActor
{
	GENERATED_BODY()

public:
	FOnEatSignature OnEat;
	FOnCollisionSignature OnCollision;

	ASnakeBodyElement();

	bool IsHead() const { return bHead; }
	void SetHead(const bool IsHead);
	void Move(const FVector& NewLocation);
	void Eat(const int32 ScoreCount, const bool AddElement) const;
	void GameOver();
	void SetColor(const FSnakeColorData& ColorData) const;
	bool CanDestroyWall() const;

	const FVector& GetPreviewLocation() const { return PreviewLocation; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Components")
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="BodyElement|Color")
	FName ColorParameterName = "BaseColor";

	virtual void BeginPlay() override;

private:
	bool bHead = false;
	FVector PreviewLocation;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	              AActor* OtherActor,
	              UPrimitiveComponent* OtherComp,
	              int32 OtherBodyIndex,
	              bool bFromSweep,
	              const FHitResult& SweepResult);
};
