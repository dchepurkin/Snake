// Created by Dmitriy Chepurkin, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "SnakeCoreTypes.h"
#include "GameFramework/Pawn.h"
#include "SnakePlayerPawn.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnAddElementSignature, const int32);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTakeSpeedBonusSignature);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTakeDestroyWallBonusSignature);

class ASnakeBodyElement;

UCLASS()
class SNAKE_API ASnakePlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	FOnAddElementSignature OnAddElement;

	UPROPERTY(BlueprintAssignable)
	FOnTakeSpeedBonusSignature OnTakeSpeedBonus;

	UPROPERTY(BlueprintAssignable)
	FOnTakeDestroyWallBonusSignature OnTakeDestroyWallBonus;

	ASnakePlayerPawn();

	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void ApplySpeedBonus(const float SpeedFactor, const float BonusTime);
	void ApplyDestroyWallBonus(const float BonusTime);

	void AddSnakeElement(const int32 ElementsCount = 1);

	UFUNCTION(BlueprintCallable)
	float GetLifeTimePercent() const { return CurrentLifeTime / SnakeLifeTime; }

	UFUNCTION(BlueprintCallable)
	int32 GetScore() const { return CurrentScore; }

	UFUNCTION(BlueprintCallable)
	bool HaveSpeedBonus() const { return bIsHaveSpeedBonus; }

	UFUNCTION(BlueprintCallable)
	float GetSpeedBonusTime() const { return SpeedBonusTime; }

	UFUNCTION(BlueprintCallable)
	bool HaveDestroyWallBonus() const { return bIsHaveDestroyWallBonus; }

	UFUNCTION(BlueprintCallable)
	float GetDestroyWallBonusTime() const { return DestroyWallBonusTime; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Snake|Movement", meta=(ClampMin = 0.01, ToolTip="Interval between snake movements"))
	float StartMoveTickInterval = 0.15f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Snake|Movement", meta=(ClampMin = 0.01))
	float MinMoveTickInterval = 0.05f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Snake|Movement", meta=(ClampMin = 0.01))
	float MaxMoveTickInterval = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Snake|Movement")
	EMovementDirection StartMovementDirection = EMovementDirection::MD_Up;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Snake")
	TSubclassOf<ASnakeBodyElement> BodyElementClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Snake", meta=(ClampMin=3))
	int32 StartElementsCount = 4;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Snake")
	float BodyElementSize = 50.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Snake", meta=(ClampMin=1.f))
	float SnakeLifeTime = 15.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Snake")
	USoundBase* DeathSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Snake|Color")
	FSnakeColorData DefaultSnakeColor;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Snake|Color")
	FSnakeColorData DestroyWallSnakeColor;

	virtual void BeginPlay() override;

private:
	FTimerHandle LifeTimeTimerHandle;
	float CurrentLifeTime = 0.f;

	bool bIsGameStarted = false;
	bool bIsGameOver = false;

	bool bIsHaveDestroyWallBonus = false;
	float DestroyWallBonusTime = 0.f;

	bool bIsHaveSpeedBonus = false;
	float SpeedBonusTime = 0.f;

	int32 CurrentScore = 0;

	EMovementDirection CurrentMovementDirection;
	EMovementDirection PreviewMovementDirection; //���������� ����������� ��������, ����� ��������� ��� ������������ ������ ������	
	TArray<ASnakeBodyElement*> BodyElements;

	TMap<EMovementDirection, FVector> DirectionVectors{
		{EMovementDirection::MD_Up, FVector(1.f, 0.f, 0.f)},
		{EMovementDirection::MD_Down, FVector(-1.f, 0.f, 0.f)},
		{EMovementDirection::MD_Right, FVector(0.f, 1.f, 0.f)},
		{EMovementDirection::MD_Left, FVector(0.f, -1.f, 0.f)}
	};

	UFUNCTION()
	void OnGameOver();

	void StartGame();
	void SnakeLifeTimeTick();
	void DestroySnake();
	void UpdateSnakeColor();

	void SetMovementDirection(const EMovementDirection NewDirection);
	void MoveForward(const float AxisValue);
	void MoveRight(const float AxisValue);
	void MoveTick();

	void OnEat(const int32 ScoreCount, const bool AddElement);
	void SpeedBonusTick(const float DeltaSeconds);
	void DestroyWallBonusTick(const float DeltaSeconds);
};
