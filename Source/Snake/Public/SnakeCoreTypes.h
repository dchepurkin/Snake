#pragma once

#include "SnakeCoreTypes.generated.h"

UENUM()
enum class EMovementDirection : uint8
{
	MD_Up UMETA(DisplayName = "Up"),
	MD_Down UMETA(DisplayName = "Down"),
	MD_Right UMETA(DisplayName = "Right"),
	MD_Left UMETA(DisplayName = "Left")
};

USTRUCT(BlueprintType)
struct FFoodData
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UStaticMesh* Mesh = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName ColorParameterName = "BaseColor";

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FLinearColor Color = FLinearColor::Blue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 Score = 10;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UMaterialInstance* LifeTimeProgressBarMaterial = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USoundBase* EatSound = nullptr;
};

USTRUCT(BlueprintType)
struct FSnakeColorData
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FLinearColor HeadColor = FLinearColor::Gray;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FLinearColor BodyColor = FLinearColor::Gray;	
};
