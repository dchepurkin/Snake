// Created by Dmitriy Chepurkin, All Rights Reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SnakeCircleProgressBar.generated.h"

class UImage;

UCLASS()
class SNAKE_API USnakeCircleProgressBar : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetMaterial(UMaterialInstance* NewMaterial);
	void SetPercent(const float NewPercent) const;

protected:
	UPROPERTY(BlueprintReadOnly, meta=(BindWidget))
	UImage* LifeTimeProgressBar;

private:
	UPROPERTY()
	UMaterialInstanceDynamic* CurrentMaterial = nullptr;

	const FName PercentParameterName = "Percent";
};
